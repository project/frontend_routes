# Frontend Routes

Frontend Routes is designed to improve the [Drupal aletr event route](https://www.drupal.org/docs/drupal-apis/routing-system/altering-existing-routes-and-adding-new-routes-based-on-dynamic-ones). 
We provide a form that creates and gives you the ability 
to change the routes you want on your site.

## Table of contents

- Installation
- Configuration
- Maintainers

## Installation

Install as you would normally install a contributed Drupal module.

## Configuration

Configure the admin routes settings at (/admin/config/system/routes-settings).

## Maintainers

Current maintainers:

- [Igor Morgun (gR3m4ik)](https://www.drupal.org/u/gr3m4ik)
- [Roman Deiloff (deiloff)](https://www.drupal.org/u/deiloff)

Supporting organizations:

- [DINAMIKA](https://www.drupal.org/dinamika) Created this module for you!