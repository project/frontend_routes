<?php

namespace Drupal\frontend_routes\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

/**
 * Configure Frontend routes settings for this site.
 */
class RoutesSettingsForm extends ConfigFormBase
{

    /**
     * Config Setting Name
     */
    const SETTINGS = 'frontend_routes.settings';

    /**
     * Default Routes List
     */
    private $routes = [
        'user.page',
        'user.login',
        'user.logout',
        'user.cancel_confirm',

        'entity.user.canonical',
        'entity.user.edit_form,',
        'entity.user.cancel_form',

        'views.ajax',

        'viewsfilters.autocomplete',
        'views_bulk_operations.confirm',
        'views_bulk_operations.execute_batch',
        'views_bulk_operations.execute_configurable',
        'views_bulk_operations.update_selection',


        'system.entity_autocomplete',

        'editor.filter_xss',
        'editor.image_dialog',
        'editor.link_dialog',

        'ckeditor5.media_entity_metadata.23',
        'ckeditor5.upload_image'
    ];

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'frontend_routes_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames()
    {
        return [static::SETTINGS];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $routes = $this->getRoutes();

        $form['routes_input'] = [
            '#type' => 'container',
            '#tree' => TRUE,
        ];

        $form['routes'] = [
            '#type' => 'container',
            '#tree' => TRUE,
        ];

        foreach ($routes as $group_name => $route_group) {

            $form['routes'][$group_name] = [
                '#type' => 'details',
                '#open' => TRUE,
                '#title' => $group_name,
            ];

            foreach ($route_group as $route_name => $route) {

                $form['routes'][$group_name][$route_name] = array(
                    '#type' => 'fieldset',
                    '#title' => $route_name,
                    '#description' => 'path: ' . $route['path']
                );

                $form['routes'][$group_name][$route_name]['path'] = array(
                    '#type' => 'hidden',
                    '#value' => $route['path'],
                );

                $form['routes'][$group_name][$route_name]['checkbox'] = array(
                    '#type' => 'checkbox',
                    '#title' => $this->t('Eanble new path?'),
                    '#default_value' => $route['checkbox'] ?? false,
                );

                $form['routes'][$group_name][$route_name]['new_path'] = array(
                    '#type' => 'textfield',
                    '#default_value' => $route['new_path'] ?? $route['path'],        // custom path
                    '#states' => [
                        'visible' => [
                            [':input[name="checkbox"]' => ['value' => true]],
                        ],
                    ],
                );

            }

        }

        $form['actions']['reset'] = [
            '#type'   => 'submit',
            '#value'  => t('Reset'),
            '#submit' => array('::resetForm'),
            '#weight' => 1,
        ];

        return parent::buildForm($form, $form_state);
    }

    public function resetForm() {
        $config = $this->config(static::SETTINGS);

        if ($config->getOriginal()) {
            $config->delete();
        }
    }

    /**
     * Get array Routes
     */
    private function getRoutes()
    {
        $config = $this->config(static::SETTINGS);
        $config_routes = $config->get('routes') ?? [];

        $route_provider = \Drupal::service('router.route_provider');

        //Render Static Routes
        $routes = [];
        foreach ($this->routes as $key => $route_name) {
            $parts = explode('.', $route_name, 2);

            try {
                $path = $route_provider->getRouteByName($route_name);

                $routes[$parts[0]][$route_name] = [
                    'checkbox' => false,
                    'path' => $path->getPath(),
                    'new_path' => $path->getPath(),
                ];

            } catch (RouteNotFoundException $exception) {
                //                      return NULL;
            }
        }

        // replace base routes on your configs data
        $result = array_merge_recursive($routes, $config_routes);

        $routes_c = $this->dotToUnderlining($result, '__', '.');

        return $routes_c;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        parent::validateForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $routes = $form_state->getValue('routes') ?? [];

        // error schema with .
        // We replace . on __
        $routes_c = $this->dotToUnderlining($routes);

        $this->config(static::SETTINGS)
            ->set('routes', $routes_c)
            ->save();

        // Clear all plugin caches.
        \Drupal::service('plugin.cache_clearer')->clearCachedDefinitions();
        // Rebuild routers based on all rebuilt data.
        \Drupal::service('router.builder')->rebuild();

        parent::submitForm($form, $form_state);
    }

    private function dotToUnderlining($routes, $s = '.', $ss = '__')
    {
        $updated = [];
        foreach ($routes as $key => $route) {
            foreach ($route as $keyy => $routee) {
                $updated_key = str_replace($s, $ss, $keyy);
                $updated[$key][$updated_key] = $routee;
            }
        }

        return $updated;
    }

}