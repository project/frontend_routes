<?php

declare(strict_types=1);

namespace Drupal\frontend_routes\EventSubscriber;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Frontend routes event subscriber.
 *
 * inside:
 * https://www.drupal.org/docs/drupal-apis/routing-system/altering-existing-routes-and-adding-new-routes-based-on-dynamic-ones
 */
class FrontendRoutesSubscriber extends RouteSubscriberBase {

    /**
     * {@inheritdoc}
     */
    protected function alterRoutes(RouteCollection $collection): void {
        $config = \Drupal::config('frontend_routes.settings');
        $routes = $config->get('routes');

        foreach ($routes as $group_name => $route_group) {

            foreach ($route_group as $route_name => $route) {
                if ($route['checkbox']) {
                    $name = str_replace('__', '.', $route_name);

                    if ($existingRoute = $collection->get($name)) {
                        $existingRoute->setPath($route['new_path']);
                    }
                }
            }
        }
    }

}
